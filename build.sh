#!/usr/bin/env sh

set -e
set -u

hugo \
      --baseURL "$1" \
      --buildFuture \
      --cleanDestinationDir \
      --minify