#-----------------------------------------
# Variables
#-----------------------------------------
MKFILE_PATH := $(abspath $(lastword ${MAKEFILE_LIST}))
PROJECT_PATH := $(dir ${MKFILE_PATH})
PROJECT_NAME := $(shell basename ${PROJECT_PATH})
export PROJECT_NAME
PROJECT_URL=blog_dolmenpublic.docker.localhost
export PROJECT_URL
UID=$(shell id -u)
export UID
GID=$(shell id -g)
export GID

# command name that are also directories
.PHONY: dev

#-----------------------------------------
# Allow passing arguments to make
#-----------------------------------------
SUPPORTED_COMMANDS := dev run
SUPPORTS_MAKE_ARGS := $(findstring $(firstword $(MAKECMDGOALS)), $(SUPPORTED_COMMANDS))
ifneq "$(SUPPORTS_MAKE_ARGS)" ""
  COMMAND_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(COMMAND_ARGS):;@:)
endif

#-----------------------------------------
# Help commands
#-----------------------------------------
.PHONY: help
.DEFAULT_GOAL := help

help: ## Prints this help
	@grep -E '^[a-zA-Z_\-\0.0-9]+:.*?## .*$$' ${MAKEFILE_LIST} | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-30s\033[0m %s\n", $$1, $$2}'

#-----------------------------------------
# Commands
#-----------------------------------------
clean: ## Cleans up environnement
	@docker-compose down --remove-orphans
	@docker-compose build --pull

dev: clean ## Starts dev stack
	@docker-compose up -d
	@echo "https://blog_dolmenpublic.docker.localhost"

run: ## Build blog
	@docker-compose run --rm site bash -c "/src/build.sh 'https://${PROJECT_URL}'"

sh: ## Launch hugo assisted shell
	@docker-compose run --rm site shell

build: ## Build docker image
	@docker build .

kube.lint: ## Lint helm chart
	@docker-compose -f docker-compose.kube.yml run --rm kubectl
	@docker-compose -f docker-compose.kube.yml run --rm kubectl -c "helm template --debug chart"
