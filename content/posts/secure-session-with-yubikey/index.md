+++
date = 2021-05-20T08:00:00Z
author = "Goulven"
title = "Sécuriser son poste Linux avec une Yubikey"
summary = "Comment gagner en sécurité et confort d'utilisation sur un environnement Linux grâce à une Yubikey."
feature = "secure-session-with-yubikey/images/illustration.jpg"
tags = ["linux", "tech", "yubikey"]
+++

La sécurité dans le monde de l'IT est un vaste sujet. Sécurisation des infrastructures, des applications, des données... il y en a pour tous les goûts.

Aujourd'hui, je vous propose de gagner en sécurité **et** en confort d'utilisation sur votre environnement Linux grâce à une Yubikey.

## 1. C'est quoi une Yubikey ?
{{< blockquote quote="La YubiKey est un dispositif d'authentification électronique fabriqué par Yubico qui supporte les mots de passe à usage unique, le chiffrement et l'authentification par clé publique et le protocole Universal Second Factor développé par l'alliance FIDO." attr=" " cite="Wikipedia" link="https://fr.wikipedia.org/wiki/YubiKey" >}}

En clair, il s'agit d'une clé USB physique, sauf qu'au lieu de stocker des fichiers, elle contient une puce sécurisée qui renferme une clé chiffrée unique qui **vous appartient**.

Elle repose sur un standard ouvert nommé {{< abbr title="Universal Two-Factor" text="U2F" >}} qui a été développé par Google, Yubico et NXP et qui est maintenu par [l'alliance FIDO](https://fidoalliance.org/).

{{< alert "En ce qui me concerne, j'utilise une Yubikey 5 NFC." "info" >}}

## 2. Utiliser la Yubikey pour les commandes `sudo`
Le but de la manoeuvre ici est de n'avoir plus qu'à toucher sa Yubikey du doigt pour valider une commande `sudo`. Plus de mot de passe à taper !  

### 2.1 Installation des dépendances

```bash
sudo add-apt-repository ppa:yubico/stable && sudo apt-get update
sudo apt-get install libpam-u2f pamu2fcfg
```

### 2.2 Associer la clé U2F avec son compte

1. Ouvrir un terminal
1. Brancher la Yubikey
1. Exécuter les commandes suivantes :

```bash
mkdir -p ~/.config/Yubico
pamu2fcfg > ~/.config/Yubico/u2f_keys
```


Quand la Yubikey clignote toucher la partie métallique.

### 2.3 Configurer le système pour utiliser les clés U2F

```bash
sudo nano /etc/pam.d/common-auth
```

**Avant toute instruction auth**, insérer la ligne suivante :

```
auth       sufficient   pam_u2f.so
```

Sauvegarder le fichier et retirer la Yubikey.


On va maintenant vérifier que la commande `sudo` fonctionne normalement. Ouvrir un nouveau terminal et taper la commande suivante :

```bash
sudo echo test
```

Quand demandé, saisir le mot de passe et valider par {{< kbd "Entrer">}}. La commande doit normalement bien s’exécuter.

Maintenant, brancher la Yubikey et ouvrir un nouveau terminal. Rejouer la commande de test ci-dessus. La Yubikey doit se mettre à clignoter.

![Touch Yubikey](/secure-session-with-yubikey/images/touch_yubikey.jpg)

Toucher la partie métallique et la commande doit s’exécuter avec succès, sans avoir à saisir son mot de passe.

{{< alert "Il est possible d'autoriser l'exécution d'une commande `sudo` dès que la Yubikey est branchée, sans avoir à toucher la partie métallique. Cependant, je vous le déconseille fortement car alors n'importe quel script peut exécuter une commande `sudo` sans même que vous le sachiez." "info" >}}


## 3. Verrouiller la session quand la Yubikey est débranchée
Le but de la manoeuvre est de verrouiller automatiquement la session lorsque la Yubikey est débranchée.  
De cette manière, dès qu'on part en pause café, il suffit de prendre sa Yubikey avec soit et plus aucun risque de devoir payer les croissants aux collègues le lendemain 🥐

On va commencer par créer le script de verrouillage : `sudo nano /usr/local/bin/lockscreen.sh` :
```bash
#!/bin/sh

# This script, when called, locks session if yubikey is absent

sleep 2

if ! ykman info >> /dev/null 2>&1
then
loginctl lock-sessions
fi
```

Ensuite, on rend le script exécutable :
```bash
sudo chmod +x /usr/local/bin/lockscreen.sh
```

Puis, on ajoute une nouvelle règle [UDEV](https://fr.wikipedia.org/wiki/Udev) :
`sudo nano /etc/udev/rules.d/20-yubikey.rules`

```bash
ACTION=="remove", ENV{SUBSYSTEM}=="usb", ENV{PRODUCT}=="1050/407/526", RUN+="/usr/local/bin/lockscreen.sh"
```

La valeur du paramètre `ENV{PRODUCT}` peut-être déterminée de cette manière :

```
# Brancher la Yubikey
# monitor les actions systèmes
udevadm monitor --environment --udev

# Débrancher la yubikey, puis CTRL + C pour stopper le monitoring
```

Dans la sortie, trouver un bloc de lignes contenant la ligne `ID_VENDOR=Yubico` et contenant les entrées `ID_VENDOR_ID`, `ID_MODEL_ID` et `ID_REVISION`.
Concatener ces 3 dernières valeurs, sans les 0 de gauche, séparés par des `/`.
Par exemple, avec les lignes suivantes vous obtiendrez `1050/407/526` :

```
ID_VENDOR=Yubico
ID_VENDOR_ID=1050
ID_MODEL_ID=0407
ID_REVISION=0526
```

Enfin, recharger la conf :

```bash
sudo udevadm control --reload-rules
```

## Aller plus loin
Sur certains services tels que GitLab, Gmail ou encore Bitwarden, vous pouvez activer l'authentification multi-facteurs et utiliser votre yubikey comme périphérique U2F.  
Et comme la Yubikey supporte le NFC, vous pouvez même l'utiliser avec un smartphone.

Vous pouvez également utiliser votre Yubikey pour chiffrer votre disque dur : sans cette dernière, il est alors impossible de le déchiffrer.

Source : [https://support.yubico.com/support/solutions/articles/15000011356-ubuntu-linux-login-guide-u2f](https://support.yubico.com/support/solutions/articles/15000011356-ubuntu-linux-login-guide-u2f)
