+++
date = 2021-04-05T08:00:00Z
author = "Goulven"
title = "Live boot Linux Mint avec partition de stockage"
summary = "Création d'un live boot linux avec partition de stockage persistant."
feature = "partitioned-bootable-usb/images/illustration.jpg"
tags = ["linux", "tech"]
+++

Bonjour amis techos, aujourd'hui nous allons voir comment créer une clé USB bootable avec une partition pour le stockage.

## C'est quoi un _live boot_ ?
Le concept d'un _live boot_ est relativement simple. Avec une distribution live Linux vous pouvez booter depuis un CD/DVD ou depuis une clé USB et choisir d'essayer l'OS sans installer quoi que ce soit sur votre disque dur.

Techniquement, le système est entièrement chargé en RAM. Le système d'exploitation et tous les programmes sont utilisables, mais exécutés depuis la RAM.
Grâce à ça, vous pouvez utiliser/tester le système aussi longtemps que vous le voulez puis, rebooter pour retourner sur l'<abbr title="Operating System">OS</abbr> original de la machine.

## Pourquoi utiliser un _live boot_ ?
Une distribution live peut être utilisée pour plusieurs raisons :

* Tester une distribution Linux : le meilleur moyen de voir si Linux est fait pour vous.
* Tester le _hardware_ : si vous n'êtes pas sûr que votre matériel va marcher avec Linux, _live bootez_ et vous verrez bien.
* Débuguer : les distrib live sont livrées avec pas mal d'outils bien utiles en cas de problème :
  * récupération de données
  * récupération de système
  * réparation de boot
  * etc

Toutes les distributions Linux ne sont pas compatibles _live boot_. Aussi, voici une liste complète des distrib ayant des releases _live_ : [https://en.wikipedia.org/wiki/List_of_live_CDs](https://en.wikipedia.org/wiki/List_of_live_CDs).

## Pourquoi partitionner une clé USB bootable ?
Une distrib bootable ne pèse que quelques Go voir moins de 2 Go. Aujourd'hui les clés USB font facilement 32 Go et plus. En en faisant une clé USB bootable, on va avoir pas loin de 30 Go d'espace inutilisé.
De plus, dans le cas d'un dépannage, il peut être pratique d'embarquer quelques outils maison ou de faire des copies de fichiers, des screenshots, etc.

Partitionner la clé USB va permettre d'avoir d'un côté le _live boot_ et de l'autre un stockage persistant et ainsi utiliser tout l'espace disponible de la clé USB.

## Créer un _live boot_ partitionné
Pour ça, vous allez avoir besoin de :
* Une clé USB
* L'utilitaire [mkusb](https://help.ubuntu.com/community/mkusb)
* Une distribution live Linux. Ici, je vais utiliser [Linux Mint](https://www.linuxmint.com/download.php)

```bash
sudo add-apt-repository ppa:mkusb/ppa  # and press Enter
sudo apt update
sudo apt install -y mkusb usb-pack-efi
```

Lancer `mkusb-plug`:

```bash
mkusb-plug
```

Dans la fenêtre qui s'ouvre, choisir l'ISO de Linux Mint fraîchement téléchargé.  
![Pick ISO](/partitioned-bootable-usb/images/mkusb-plug01.jpg)

Ensuite, sélectionner "Live drive with 'usbdata' partition" puis "Valider".
![Chose live boot type](/partitioned-bootable-usb/images/mkusb-plug02.jpg)

Choisir ensuite le système de fichier, par défaut [NTFS](https://en.wikipedia.org/wiki/NTFS).
![Pick file system](/partitioned-bootable-usb/images/mkusb-plug03.jpg)

À la question "Do you expect the drive to contain filesystems other than VFAT and ISO 9600?" sélectionner "No".
![FVAT and ISO 9600](/partitioned-bootable-usb/images/mkusb-plug04.jpg)

Ensuite, c'est le moment de s'assurer que la clé USB n'est **pas** (encore) branchée.
![Step 1](/partitioned-bootable-usb/images/mkusb-plug05-1.jpg)

Maintenant, brancher la clé USB afin que `mkusb` la détecte puis, valider.
![Step 2](/partitioned-bootable-usb/images/mkusb-plug05-2.jpg)

Une fenêtre donnant les détails du périphérique qui va être utilisé s'affiche. Vérifier qu'il s'agit de la bonne clé USB et valider.  
Dans mon cas, il s'agit bien de la _SanDisk Cruzer Blade_.
![Step 3](/partitioned-bootable-usb/images/mkusb-plug05-3.jpg)

Une dernière fenêtre de confirmation s'affiche. Sélectionner "Yes, I want to go ahead" et cliquer sur "Go".
![Confirm](/partitioned-bootable-usb/images/mkusb-plug06.jpg)

Le processus est lancé, il n'y a plus qu'à attendre.
![Process window](/partitioned-bootable-usb/images/mkusb-plug07.jpg)
![Process shell](/partitioned-bootable-usb/images/mkusb-plug08.jpg)

Finalement, retirer la clé USB, rebrancher et <kbd>Entrée</kbd>
![Final step](/partitioned-bootable-usb/images/mkusb-plug09.jpg)
Et voilà, une clé USB bootable avec une partition de stockage !