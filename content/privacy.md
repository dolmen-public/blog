# Mentions légales
## Éditeur du blog

Blog écrit par les auteurs des différents articles.

## Hébergement

[Gitlab Pages on gitlab.com](https://docs.gitlab.com/ee/user/project/pages/)

## Propriété

L'ensemble des contenus de ce blog sont publiés sous la licence [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).
![Creative Commons License](https://i.creativecommons.org/l/by-sa/4.0/88x31.png)