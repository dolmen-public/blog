# Technical choices

## Choice of static site generator : Hugo

### The plan

To run our blog, we considered several options:

* Build one from scratch using symfony
* Use a static-site generator

As long as we want a static site, Symfony was quickly discarded.

### The search

We used [this article](https://www.ample.co/blog/questions-to-ask-before-choosing-a-static-site-generator) as source to compare the different static-site generators.

We considered Hugo, Jekyll and Middleman.

* _Middleman_ was discarded after troubles to run it properly via docker.
* Hugo was a clear winner regarding speed of generation. 
* Jekyll is in a stable release (v4.2.0) whereas Hugo has no stable release (v0.82.0).
* Hugo uses Go template which will be a good training for Helm template too.

### The winner

Hugo

## Theme 

Current theme is [Axiom](https://www.axiomtheme.com/).

See [Documentation](https://www.axiomtheme.com/docs/)
